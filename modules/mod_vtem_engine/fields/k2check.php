<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
// no direct access
defined('_JEXEC') or die();
jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldK2check extends JFormField
{
    public function getInput()
    {   
	    $doc = JFactory::getDocument();
		$module = $this->form->getValue('module');
		$jversion = new JVersion;
		if (version_compare($jversion->getShortVersion(), '3.0.0', '<')){  
        $doc->addScript(JURI::root(true).'/modules/'.$module.'/styles/jquery-1.7.2.min.js');
		}
        $doc->addStyleSheet(JURI::root(true).'/modules/'.$module.'/admin/style.css');
		$doc->addScript(JURI::root(true).'/modules/'.$module.'/admin/script.js');
		
        if (defined('K2_CHECK')) return;
        $k2 = JPATH_SITE . "/components/com_k2/k2.php";

        if (!file_exists($k2)) {
            define('K2_CHECK', 0);
            $warning_style = "style='background: #FFF3A3;border: 1px solid #E7BD72;color: #B79000;display: block;padding: 8px 10px;text-align:center;'";
            return "<span $warning_style><strong>K2 Component</strong> Not Found. In order to use the <strong>K2 Content</strong> type, you will need to <a href=\"http://k2.joomlaworks.gr\" target=\"_blank\">download and install it</a>.</span>";
        } else {
            define('K2_CHECK', 1);
            return "";
        }
    }
}