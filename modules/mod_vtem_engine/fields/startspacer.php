<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/

// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldStartspacer extends JFormField
{
    protected $type = 'startspacer';
    protected function getInput()
    {
		$jversion = new JVersion;
        if ($this->name) {
            $class = $this->element['class'];
			if (version_compare($jversion->getShortVersion(), '3.0.0', '<')){          		
            	$paneOpen = '</li><fieldset class="clearfix acc_container '.$class.'" style="width:100%"><legend>'.$this->getLabel().'</legend>';
			}else{
            	$paneOpen = '</div></div><div class="clearfix acc_container '.$class.'" style="width:100%"><div class="block-label"><div>'.$this->getLabel();
			}
            return $paneOpen;
        } else {
            return '<hr />';
        }
    }
	protected function getLabel()
	{
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;
		return '<span class="vt-label">'.$text.'</span>';
	}
}
