<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/

// no direct access
defined( '_JEXEC' ) or die('Restricted access');
class modVtemengineHelper{
    public static function getLists($params){	
        $db         = JFactory::getDBO();
        $user       = JFactory::getUser();
        $userId     =  (int) $user->get('id');
        $aid        =  $user->get('aid', 0);
        $nullDate   =  $db->getNullDate();
        $date       = JFactory::getDate();
        $now        =  $date->toSql();
		$app = JFactory::getApplication();
		$user_id = $params->get('user_id');
        
        $content_source = $params->get('content_source','mods');
        //joomla specific
        if($content_source == 'joomla'){
			 require_once JPATH_SITE.'/components/com_content/router.php';
			 require_once JPATH_SITE.'/components/com_content/helpers/route.php';
			 JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');
				// Get an instance of the generic articles model
				$articles = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));
				// Set application parameters in model
				$appParams = $app->getParams();
				$articles->setState('params', $appParams);
				// Set the filters based on the module params
				$articles->setState('list.start', 0);
				$articles->setState('list.limit', (int) $params->get('countItems', 3));
				$articles->setState('filter.published', 1);
				// Access filter
				$access = !JComponentHelper::getParams('com_content')->get('show_noauth');
				$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
				$articles->setState('filter.access', $access);
				$catids = $params->get('catid');
				$articles->setState('filter.category_id.include', (bool) $params->get('category_filtering_type', 1));
				// Category filter
				if ($catids) {
				 if ($params->get('show_child_category_articles', 0) && (int) $params->get('levels', 0) > 0) {
					// Get an instance of the generic categories model
					$categories = JModelLegacy::getInstance('Categories', 'ContentModel', array('ignore_request' => true));
					$categories->setState('params', $appParams);
					$levels = $params->get('levels', 1) ? $params->get('levels', 1) : 9999;
					$categories->setState('filter.get_children', $levels);
					$categories->setState('filter.published', 1);
					$categories->setState('filter.access', $access);
					$additional_catids = array();
	
					foreach($catids as $catid)
					{
						$categories->setState('filter.parentId', $catid);
						$recursive = true;
						$items = $categories->getItems($recursive);
	
						if ($items)
						{
							foreach($items as $category)
							{
								$condition = (($category->level - $categories->getParent()->level) <= $levels);
								if ($condition) {
									$additional_catids[] = $category->id;
								}
	
							}
						}
					}
	
					$catids = array_unique(array_merge($catids, $additional_catids));
				 }
	
				$articles->setState('filter.category_id', $catids);
			  }
			// Ordering
			$articles->setState('list.ordering', $params->get('article_ordering', 'a.ordering'));
			$articles->setState('list.direction', $params->get('article_ordering_direction', 'ASC'));
			// New Parameters
			$articles->setState('filter.featured', $params->get('show_front', 'show'));
			$articles->setState('filter.author_id', $params->get('created_by', ""));
			$articles->setState('filter.author_id.include', $params->get('author_filtering_type', 1));
			$article_filtering_type = $params->get('article_filtering_type', 1);
			$article_ids = $params->get('article_ids', '');
			if($article_filtering_type == 1){
			  if ($article_ids) {
				$article_ids = explode(",", $article_ids);
				$articles->setState('filter.article_id', $article_ids);
				$articles->setState('filter.article_id.include', true); 
			  }
			}else{
				$article_ids = explode(",", $article_ids);
				$articles->setState('filter.article_id', $article_ids);
				$articles->setState('filter.article_id.include', false); 
			}
			$items = $articles->getItems();
			
			// Display options
			$showtitle = $params->get('showtitle', 0);
			$show_date = $params->get('show_date', 0);
			$show_date_field = $params->get('show_date_field', 'created');
			$show_date_format = $params->get('show_date_format', 'Y-m-d H:i:s');
			$show_category = $params->get('show_category', 0);
			$show_hits = $params->get('show_hits', 0);
			$show_author = $params->get('show_author', 0);
			$show_introtext = $params->get('show_introtext', 0);
			$introtext_limit = $params->get('introtext_limit', 100);
			$link_titles = $params->get('link_titles', 1);
			$image_type = $params->get('image_type', 'image_article');
	        $module_type = $params->get('module_type' , 'slideshow');
			
			// Find current Article ID if on an article page
			$option = JRequest::getCmd('option');
			$view = JRequest::getCmd('view');
	
			if ($option === 'com_content' && $view === 'article') {
				$active_article_id = JRequest::getInt('id');
			}
			else {
				$active_article_id = 0;
			}
			// Prepare data for display using display options
			$i = 0;
			$lists = array();
			foreach ($items as &$item)
			{
					$item->slug = $item->id.':'.$item->alias;
					$item->catslug = $item->catid ? $item->catid .':'.$item->category_alias : $item->catid;
					$lists[$i] = new stdClass();
					if ($access || in_array($item->access, $authorised)) {
						// We know that user has the privilege to view the article
						$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug));
					}
					 else {
					// Angie Fixed Routing
					$menu	= $app->getMenu();
					$menuitems	= $menu->getItems('link', 'index.php?option=com_users&view=login');
				    if(isset($menuitems[0])) {
						$Itemid = $menuitems[0]->id;
					} else if (JRequest::getInt('Itemid') > 0) { //use Itemid from requesting page only if there is no existing menu
						$Itemid = JRequest::getInt('Itemid');
					}
					$lists[$i]->link = JRoute::_('index.php?option=com_users&view=login&Itemid='.$Itemid);
					}
					
					$item->displayImage = '';
					$images = json_decode($item->images);
					if($image_type == 'image_full' && (isset($images) && $images->image_fulltext!= '')) {
						$item->displayImage = $images->image_fulltext;
					}elseif($image_type == 'imgae_intro' && (isset($images) && $images->image_intro!='')) {
						$item->displayImage = $images->image_intro;
					}elseif($image_type == 'image_article'){
						$imgSPos = JString::strpos($item->introtext,'src="');
						  if($imgSPos){
							 $imgEPos = JString::strpos($item->introtext,'"',$imgSPos+5);
							 } 
						  if($imgSPos > 0) {
							 $item->displayImage = JString::substr($item->introtext, ($imgSPos+5), ($imgEPos-($imgSPos+5)));
						   }
					}
					$lists[$i]->title = $item->title;
					$lists[$i]->introtext = '';
					if($module_type == 'slideshow' || ($module_type == 'scroller' && $image_type != 'none')){
						if($item->displayImage)
							$lists[$i]->introtext .='<a href="'.$lists[$i]->link.'"><img class="vtem_engine_image" src="'.JURI::root().$item->displayImage.'" /></a>';
						else
							$lists[$i]->introtext .='<img class="vtem_skitter_image" src="'.JURI::root().'modules/mod_vtem_engine/styles/images/noimage.jpg" />';
					}
					
					$lists[$i]->introtext .='<div class="label_text clearfix">';
					if($showtitle){
						if($link_titles){
						  $lists[$i]->introtext .= '<h4 class="vtem_engine_title"><a href="'.$lists[$i]->link.'">'.$item->title.'</a></h4>';
						}else{
						  $lists[$i]->introtext .= '<h4 class="vtem_engine_title">'.$item->title.'</h4>';
						}
					}
					if($show_date || $show_hits || $show_author || $show_category)
						$lists[$i]->introtext .= '<div class="vt-mod-info clearfix">';
						if ($show_date){
							if($show_date_field == 'created')
							$lists[$i]->introtext .= '<span class="vt-mod-date">'.JText::_('MOD_VTEM_ENGINE_OPTION_CREATED_VALUE').': '.JHTML::_('date', $item->$show_date_field, $show_date_format).'</span>';
							if($show_date_field == 'modified')
							$lists[$i]->introtext .= '<span class="vt-mod-date">'.JText::_('MOD_VTEM_ENGINE_OPTION_MODIFIED_VALUE').': '.JHTML::_('date', $item->$show_date_field, $show_date_format).'</span>';
							if($show_date_field == 'publish_up')
							$lists[$i]->introtext .= '<span class="vt-mod-date">'.JText::_('MOD_VTEM_ENGINE_OPTION_STARTPUBLISHING_VALUE').': '.JHTML::_('date', $item->$show_date_field, $show_date_format).'</span>';
						}
						if ($item->catid) {
							$item->displayCategoryLink = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catid));
							$lists[$i]->introtext .= $show_category ? '<a href="'.$item->displayCategoryLink.'" class="vt-mod-category">'.JText::_('MOD_VTEM_ENGINE_CATEGORY').': '.$item->category_title.'</a>' : '';
						}else{
							$lists[$i]->introtext .= $show_category ? '<span class="vt-mod-category">'.JText::_('MOD_VTEM_ENGINE_CATEGORY').': '.$item->category_title.'</span>' : '';
						}
						$lists[$i]->introtext .= $show_author ? '<span class="vt-mod-author">'.JText::_('MOD_VTEM_ENGINE_WRITTEN_BY').$item->author.'</span>' : '';
						$lists[$i]->introtext .= $show_hits ? '<span class="vt-mod-hits">'.JText::_('MOD_VTEM_ENGINE_HITS').': '.$item->hits.'</span>' : '';
					if($show_date || $show_hits || $show_author || $show_category)
						$lists[$i]->introtext .= '</div>';
					
					$lists[$i]->introtext .='<div class="vt-infor-text clearfix">';
					if ($show_introtext) {
						if ($introtext_limit != 0 || $introtext_limit == '')
							$lists[$i]->introtext .= self::truncate(self::_cleanIntrotext(JHtml::_('content.prepare', $item->introtext)), $introtext_limit);
						else
							$lists[$i]->introtext .= JHtml::_('content.prepare', $item->introtext);
					}
					$lists[$i]->introtext .= '</div>';
			    
					
				    if ($params->get('show_readmore')) :
 				  		$lists[$i]->introtext .= '<div class=" clearfix"></div><a class="vtem-tabs-readon readon btn btn-primary" href="'.$lists[$i]->link.'"><span>';
                    	if ($readmore = $params->get('readmore'.'<i class="fa fa-arrow-right">&nbsp;</i>')) :
                        	$lists[$i]->introtext .= $readmore;
                        else :
                      	    $lists[$i]->introtext .= JText::sprintf('Read more'.'<i class="fa fa-arrow-right">&nbsp;</i>');
                        endif;
					    $lists[$i]->introtext .= '</span></a>';
			        endif;
					$lists[$i]->introtext .= '</div>';
				   $i++;
			}
			return $lists;
        }else if($content_source == 'k2'){
			   // start K2 specific
				require_once(JPATH_SITE.'/components/com_k2/models/itemlist.php');
				require_once(JPATH_SITE.'/components/com_k2/helpers/route.php');
				require_once(JPATH_SITE.'/components/com_k2/helpers/utilities.php');
	
				//Initialize Variables
				$k2_category = $params->get('k2_category', array());
				$k2_children = $params->get('k2_children', 0);
				$k2_ordering = $params->get('k2_ordering', 'a.title');
				$k2_featured = $params->get('k2_featured', 1);
				$k2_image_size = $params->get('k2_image_size', 'M');
				$query = "SELECT a.*, c.name AS categoryname,c.id AS categoryid, c.alias AS categoryalias, c.params AS categoryparams";
				$query .= " FROM #__k2_items as a LEFT JOIN #__k2_categories c ON c.id = a.catid";
				$query .= " WHERE a.published = 1 AND a.access IN(" . implode(',', $user->getAuthorisedViewLevels()) . ") AND a.trash = 0 AND c.published = 1 AND c.access IN(" . implode(',', $user->getAuthorisedViewLevels()) . ")  AND c.trash = 0";
				//User Filter
				switch ($user_id){
					case 'by_me':
						$query .= ' AND (a.created_by = ' . (int)$userId . ' OR a.modified_by = ' . (int)$userId . ')';
						break;
					case 'not_me':
						$query .= ' AND (a.created_by <> ' . (int)$userId . ' AND a.modified_by <> ' . (int)$userId . ')';
						break;
				}
	
				$query .= " AND ( a.publish_up = " . $db->Quote($nullDate) . " OR a.publish_up <= " . $db->Quote($now) . " )";
				$query .= " AND ( a.publish_down = " . $db->Quote($nullDate) . " OR a.publish_down >= " . $db->Quote($now) . " )";
	
				if (!is_null($k2_category)) {
					if (is_array($k2_category)) {
						if ($k2_children) {
							$categories = K2ModelItemlist::getCategoryTree($k2_category);
							$sql = @implode(',', $categories);
							$query .= " AND a.catid IN ({$sql})";
	
						} else {
							JArrayHelper::toInteger($k2_category);
							$query .= " AND a.catid IN(" . implode(',', $k2_category) . ")";
						}
	
					} else {
						if ($k2_children) {
							$categories = K2ModelItemlist::getCategoryTree($k2_category);
							$sql = @implode(',', $categories);
							$query .= " AND a.catid IN ({$sql})";
						} else {
							$query .= " AND a.catid=" . (int)$k2_category;
						}
	
					}
				}
	
				if ($k2_featured == '0')
					$query .= " AND a.featured != 1";
	
				if ($k2_featured == '2')
					$query .= " AND a.featured = 1";
	
				if ($app->getLanguageFilter()) {
					$languageTag = JFactory::getLanguage()->getTag();
					$query .= " AND c.language IN (" . $db->Quote($languageTag) . ", " . $db->Quote('*') . ") AND a.language IN (" . $db->Quote($languageTag) . ", " . $db->Quote('*') . ")";
				}
	
				// ordering
				$ordering = $params->get('k2_itemsOrdering');
				$module_type = $params->get('module_type' , 'slideshow');
				switch ($ordering) {
					case 'date' :
						$orderby = 'a.created ASC';
						break;
					case 'rdate' :
						$orderby = 'a.created DESC';
						break;
					case 'alpha' :
						$orderby = 'a.title';
						break;
					case 'ralpha' :
						$orderby = 'a.title DESC';
						break;
					case 'order':
						if ($k2_featured == '2')
							$orderby = 'a.featured_ordering';
						else
							$orderby = 'a.ordering';
						break;
					case 'random' :
						$orderby = 'RAND()';
						break;
					default :
						$orderby = 'a.id DESC';
						break;
				}
	
				$query .= " ORDER BY " . $orderby;
				$db->setQuery($query, 0, $params->get('countItems', 3));
				$items = $db->loadObjectList();
				$i = 0;
				$lists = array();
				foreach ($items as $item)
				{
					$lists[$i] = new stdClass();
				  	$lists[$i]->id = $item->id;
				  	$link = JRoute::_(K2HelperRoute::getItemRoute($item->id, $item->catid));
				  	$readmore_register = false;
                  	$images = self::getK2Images($item->id, $k2_image_size);
				  	$lists[$i]->link = $link;
                  	$lists[$i]->readmore_register = $readmore_register;
				  	$lists[$i]->introtext = $lists[$i]->title = '';
				    if ($images)
                    	$lists[$i]->image = $images->image;
                    else
                   		$lists[$i]->image = '';
				    
				  	$lists[$i]->title = htmlspecialchars($item->title);
					//Images
				    if ($params->get('k2_itemImage')){
						if($module_type =='slideshow' && $lists[$i]->image == '')
							$lists[$i]->introtext .='<img class="vtem_skitter_image" src="'.JURI::root().'modules/mod_vtem_engine/styles/images/noimage.jpg" />';
						else if($module_type !='slideshow' && $lists[$i]->image == '')
							$lists[$i]->introtext = '';
						else
				  			$lists[$i]->introtext = '<img src="'.$lists[$i]->image.'" alt="'.$lists[$i]->title.'"/>';
					}else if(!$params->get('k2_itemImage') && $module_type =='slideshow')
						$lists[$i]->introtext .='<img class="vtem_skitter_image" src="'.JURI::root().'modules/mod_vtem_engine/styles/images/noimage.jpg" />';
						
					$lists[$i]->introtext .='<div class="label_text clearfix">';
					//Clean title
				    if ($params->get('k2_itemTitle'))
						$lists[$i]->introtext .= '<h4 class="vtem_engine_title"><a href="'.$lists[$i]->link.'">'.htmlspecialchars($item->title).'</a></h4>';
					
					if($params->get('k2_itemDateCreated') || $params->get('k2_itemCategory') || $params->get('k2_itemHits'))
						$lists[$i]->introtext .= '<div class="vt-mod-info clearfix">';
							//Created Date
							if($params->get('k2_itemDateCreated'))
								$lists[$i]->introtext .='<span class="vt-mod-date">'.JText::_('MOD_VTEM_ENGINE_OPTION_CREATED_VALUE').' : '.JHTML::_('date', $item->created, JText::_('K2_DATE_FORMAT_LC2')).'</span>';
							//Category link
							if ($params->get('k2_itemCategory')){
								$item->categoryLink = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($item->catid.':'.urlencode($item->categoryalias))));
								$lists[$i]->introtext .='<a class="vt-mod-category" href="'.$item->categoryLink.'">'.$item->categoryname.'</a>';
							}
							if($params->get('k2_itemHits'))
								$lists[$i]->introtext .='<span class="vt-mod-hits">'.JText::_('MOD_VTEM_ENGINE_HITS').': '.$item->hits.'</span>';
					if($params->get('k2_itemDateCreated') || $params->get('k2_itemCategory') || $params->get('k2_itemHits'))
						$lists[$i]->introtext .= '</div>';
						
					//Introtext
				    if ($params->get('k2_itemIntroText')){
						$lists[$i]->introtext .='<div class="vt-engine-intro clearfix">';
					    if ($params->get('k2_itemIntroTextLimit'))
					  		$lists[$i]->introtext .= K2HelperUtilities::wordLimit($item->introtext, $params->get('k2_itemIntroTextLimit'));
					    else
				            $lists[$i]->introtext .= $item->introtext;
						$lists[$i]->introtext .='</div>';
				    }
					
					//Extra fields
					$model = K2Model::getInstance('Item', 'K2Model');
					$item->extra_fields = $model->getItemExtraFields($item->extra_fields, $item);
					if($params->get('k2_itemExtraFields') && count($item->extra_fields)){
						$lists[$i]->introtext .= '<div class="vt-mod-extraFields clearfix"><ul>';
						foreach ($item->extra_fields as $extraField):
							if($extraField->value != ''){
								$lists[$i]->introtext .= '<li class="type'.ucfirst($extraField->type).' group'.$extraField->group.'">'.
									'<span class="extraFieldsLabel">'.$extraField->name.'</span>'.
									'<span class="extraFieldsValue">'.$extraField->value.'</span>'.
								'</li>';
							}
						endforeach;
						$lists[$i]->introtext .= '</ul></div>';
					}
					
					//Read more link
				    if ($params->get('show_k2readmore')) :
					  $lists[$i]->introtext .= '<a href="'.$lists[$i]->link.'" class="vtem-tabs-readon"><span>';
						 if ($lists[$i]->readmore_register) :
							$lists[$i]->introtext .= JText::_('Register to read more...');
						  elseif ($readmore = $params->get('k2readmore')) :
							 $lists[$i]->introtext .= $readmore;
						  else :
							$lists[$i]->introtext .= JText::sprintf('Read more...');
						  endif;
					  $lists[$i]->introtext .= '</span></a>';
				    endif;
					$lists[$i]->introtext .= '</div>';
				$i++;
				}
				return $lists;
        }else{
				//module specific
				if (trim($params->get('sortModuleManual')) != "")
					$mods = explode(",", $params->get('sortModuleManual'));
				else
					$mods = $params->get('modules');
				$options 	= array('style' => 'none');
				$lists = array();
				if(count($mods) > 1){
				 for($i=0; $i<count($mods); $i++){
					if($i < $params->get('countItems', 3)){
						$lists[$i] = new stdClass();
						$lists[$i]->order 	= self::getModule($mods[$i])->ordering;
						$lists[$i]->title 	= self::getModule($mods[$i])->title;
						$lists[$i]->introtext = JModuleHelper::renderModule(self::getModule($mods[$i]), $options);
					}
				 }
				}else{
					$lists[0] = new stdClass();
					$lists[0]->order 	= self::getModule($mods)->ordering;
					$lists[0]->title 	= self::getModule($mods)->title;
					$lists[0]->introtext = JModuleHelper::renderModule(self::getModule($mods), $options);
				}
				return $lists;
        }
    }
	
	public static function imageList ($directory, $sortcriteria, $sortorder) {
	    $results = array();
	    $handler = opendir($directory);
			$i = 0;
	    while ($file = readdir($handler)) {
	        if ($file != '.' && $file != '..' && self::isImage($file)) {
						$results[$i][0] = $file;
						$results[$i][1] = filemtime($directory . "/" .$file);
						$i++;
					}
	    }
	    closedir($handler);

			//these lines sort the contents of the directory by the date
			// Obtain a list of columns

			foreach($results as $res) {
				if ($sortcriteria == 0 ) $sortAux[] = $res[0];
				else $sortAux[] = $res[1];
			}

			if ($sortorder == 0) {
				array_multisort($sortAux, SORT_ASC, $results);
			} elseif ($sortorder == 2) {
				srand((float)microtime() * 1000000);
				shuffle($results);
			} else {
				array_multisort($sortAux, SORT_DESC, $results);
			}

			foreach($results as $res) {
				$sorted_results[] = $res[0];
			}

	    return $sorted_results;
	}

	public static function isImage($file) {
		$imagetypes = array(".jpg", ".jpeg", ".gif", ".png");
		$extension = substr($file,strrpos($file,"."));
		if (in_array($extension, $imagetypes)) return true;
		else return false;
	}

	public static function cleanDir($dir) {
		if (substr($dir, -1, 1) == '/')
			return $dir;
		else
			return $dir . "/";
	}
	
    //fetch module by id
    public static function getModule($id){
		$db		= JFactory::getDBO();
		$where = ' AND ( m.id='.$id.' ) ';
		$query = 'SELECT *'.
			' FROM #__modules AS m'.
			' WHERE m.client_id = 0'.
			$where.
			' ORDER BY ordering'.
			' LIMIT 1';
		$db->setQuery( $query );
		$module = $db->loadObject();
		if (!$module) return null;
		$file				= $module->module;
		$custom				= substr($file, 0, 4) == 'mod_' ?  0 : 1;
		$module->user		= $custom;
		$module->name		= $custom ? $module->title : substr($file, 4);
		$module->style		= null;
		$module->position	= strtolower($module->position);
		$clean[$module->id]	= $module;
		return $module;
	}
	
	public static function _cleanIntrotext($introtext)
	{
		$introtext = str_replace('<p>', ' ', $introtext);
		$introtext = str_replace('</p>', ' ', $introtext);
		$introtext = strip_tags($introtext, '<a><em><strong>');

		$introtext = trim($introtext);

		return $introtext;
	}

	public static function truncate($html, $maxLength = 0)
	{
		$baseLength = strlen($html);
		$diffLength = 0;

		// First get the plain text string. This is the rendered text we want to end up with.
		$ptString = JHtml::_('string.truncate', $html, $maxLength, $noSplit = true, $allowHtml = false);

		for ($maxLength; $maxLength < $baseLength;)
		{
			// Now get the string if we allow html.
			$htmlString = JHtml::_('string.truncate', $html, $maxLength, $noSplit = true, $allowHtml = true);

			// Now get the plain text from the html string.
			$htmlStringToPtString = JHtml::_('string.truncate', $htmlString, $maxLength, $noSplit = true, $allowHtml = false);

			// If the new plain text string matches the original plain text string we are done.
			if ($ptString == $htmlStringToPtString)
			{
				return $htmlString;
			}
			// Get the number of html tag characters in the first $maxlength characters
			$diffLength = strlen($ptString) - strlen($htmlStringToPtString);

			// Set new $maxlength that adjusts for the html tags
			$maxLength += $diffLength;
			if ($baseLength <= $maxLength || $diffLength <= 0)
			{
				return $htmlString;
			}
		}
		return $html;
	}
		
    public static function generateTabs($tabs, $list, $params){
        $title_type = $params->get('mod_title_type');
        $position = $params->get('tabs_position','top');
        if($title_type == 'custom'){
            $titles = explode(";",$params->get('mod_title_custom'));
        }
        if($tabs == 0 OR $tabs > count($list)) $tabs = count($list);
        $html  = "<div class='clearfix vtemtabs-nav vtemtabs-nav-$position'>";
        $html .= "<ul class='vtemtabsnav oTabs'>";
        for($i=0; $i < $tabs; $i++){
            $class = '';
            if($list[$i]->introtext != NULL){
                if(!$i) $class= 'first';
                if($i == $tabs - 1) $class= 'last';
                if($title_type == 'custom') $title = (isset($titles[$i])) ? $titles[$i] : '';
                else $title = $list[$i]->title;
                $html .= "<li class='$class'><a href='#'><span>$title</span><strong>&nbsp;</strong></a></li>\n";
            }
        }
        $html .= "</ul>\n";
        $html .= "</div>";
        return $html;  
    }

    public static function getK2Images($id, $image_size){
        if (file_exists(JPATH_SITE .'/media/k2/items/cache/'.md5("Image".$id).'_'.$image_size.'.jpg')) {
            $image_path = 'media/k2/items/cache/'.md5("Image".$id).'_'.$image_size.'.jpg';
			$images = new stdClass();
            $images->image = JURI::Root(true).'/'.$image_path;
            return $images;
        }
    }
}