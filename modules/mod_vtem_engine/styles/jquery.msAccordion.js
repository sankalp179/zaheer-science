;(function($){
	$.fn.msAccordion = function(options) {
		options = $.extend({
					currentDiv:'1',
					width:'100%',
					height:'300px',
					previousDiv:'',
					vertical: false,
					defaultid:0,
					currentcounter:0,
					intervalid:0,
					autodelay:0,
					padding: '1px',
					event:"click",
					onemustopen: true,
					autoResize: true,
					alldivs_array:new Array()
			}, options);
		$(this).addClass("accordionWrapper").parent().css({'overflow':"hidden", 'width':options.width});
		$(this).show().width($(this).parent().width());
		var elementid = $(this).attr("id");
		var allDivs = this.children().addClass('accordion-item').css({'padding': options.padding, 'display': 'inline'});
		var totalTitleWidth = allDivs.length * $(this).find('.title').outerWidth();
		var contentWidth = ($(this).width() - totalTitleWidth) - (2*parseInt(options.padding)*allDivs.length);
		if(options.autodelay>0) {
			$("#"+ elementid +" > div").bind("mouseenter", function(){ pause(); });
			$("#"+ elementid +" > div").bind("mouseleave", function(){ startPlay(); });
		}
		allDivs.each(function(current) { //set ids
								 var iCurrent = current;
								 var sTitleID = elementid+"_msTitle_"+(iCurrent);
								 var sContentID = sTitleID+"_msContent_"+(iCurrent);
								 var currentDiv = allDivs[iCurrent];
								 var titleDiv = $(currentDiv).find("div.title");
								 titleDiv.attr("id", sTitleID);
								 var contentDiv = $(currentDiv).find("div.content");
								 contentDiv.attr("id", sContentID);
								 if(!options.vertical){
									 titleDiv.height(options.height);
									 contentDiv.children().width(contentWidth);
									 $(this).find('.toggler').css({'width':parseInt(options.height)-20, 'top':parseInt(options.height)-25});
								 }
								 options.alldivs_array.push(sTitleID);
								 if(!options.onemustopen){
								 if($(currentDiv).css("display")!="none" && options.event == "click"){$("#"+sTitleID).bind(options.event,function(){pause();closeMe(sContentID);});}}
								 $("#"+sTitleID).bind(options.event, function(){pause();openMe(sTitleID);});
								 });
		if(options.vertical) {makeVertical();}; //make vertical
		openMe(elementid+"_msTitle_"+options.defaultid); //open default
		if(options.autodelay>0) {startPlay();}; //Auto Play
		if(options.autoResize){ //bind setsize function to window resize event
			$(window).resize(function(){
				resizeWidth = $('#'+elementid).parent().width();
				resizeContentWidth = (resizeWidth - totalTitleWidth) - (2*parseInt(options.padding)*allDivs.length);
				if(options.vertical)
					$('#'+elementid).width(resizeWidth).children().width(parseInt(resizeWidth)-2).find('.content').children().width(parseInt(resizeWidth)-2);
				else
					$('#'+elementid).width(resizeWidth).find('.content').children().width(resizeContentWidth);
			});
		}
		function openMe(id) {
			var sTitleID = id;
			var iCurrent = sTitleID.split("_")[sTitleID.split("_").length-1];
			options.currentcounter = iCurrent;
			var sContentID = id+"_msContent_"+iCurrent;
			    $("#"+sContentID).addClass("accordionActive");
			if($("#"+sContentID).css("display")=="none") {
				if(options.previousDiv!="") {
					closeMe(options.previousDiv);
				};
				if(options.vertical) {
					$("#"+sContentID).slideDown("slow").prev().addClass("active");
				} else {
					$("#" + sContentID + " > div").css({'display':'inline', 'height': options.height});
					$("#"+sContentID).show("slow").prev().addClass("active");
				}
				options.currentDiv = sContentID;
				options.previousDiv = options.currentDiv;
			};
		};
		function closeMe(div) {
			if(options.vertical) {
				$("#"+div).slideUp("slow").prev().removeClass("active");
			} else {
				$("#"+div).removeClass("accordionActive");
				if(options.onemustopen){
					$("#"+div).hide("slow").css({ display: "none" }).prev().removeClass("active");
				}else{
					$("#"+div).hide("slow").prev().removeClass("active");
				}
				$("#" + div + " > div").css({ display: "none" }).prev().removeClass("active");
			};
		};	
		function makeVertical() {
			$("#"+elementid +" > div").css({display:"block", float:"none", clear:"both", 'width': parseInt($('.accordionWrapper').width())-2});
			$("#"+elementid +" > div > div.title").css({display:"block", float:"none", clear:"both", 'width':'100%'});
			$("#"+elementid +" > div > div.content").css({clear:"both", 'width':'100%'}).children().width(parseInt($('.accordionWrapper').width())-2);
		};
		function startPlay() {
			options.intervalid = window.setInterval(play, options.autodelay);
		};
		function play() {
			var sTitleId = options.alldivs_array[options.currentcounter];
			openMe(sTitleId);
			options.currentcounter++;
			if(options.currentcounter==options.alldivs_array.length) options.currentcounter = 0;
		};
		function pause() {
			window.clearInterval(options.intervalid);
		};
		}
})(jQuery);