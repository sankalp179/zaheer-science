<?php

/**

 * @version		$Id: error.php 21322 2011-05-11 01:10:29Z dextercowley $

 * @package		Joomla.Site

 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.

 * @license		GNU General Public License version 2 or later; see LICENSE.txt

 */

defined('_JEXEC') or die;

if (!isset($this->error)) {

	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));

	$this->debug = false;

}

//get language and direction

$doc = JFactory::getDocument();

$this->language = $doc->language;

$this->direction = $doc->direction;



?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>

	<title><?php echo $this->error->getCode(); ?> - <?php echo $this->title; ?></title>

    <link rel="stylesheet" href="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/css/bootstrap/css/bootstrap.css" type="text/css" />

	<link rel="stylesheet" href="<?php echo $this->baseurl ;?>/templates/<?php echo $this->template ;?>/css/template.css" type="text/css" />

</head>

<body id="vtem_warper_error">

<div class="container clearfix">

<h1 class="not_found page_margin_top_section">404</h1>

<h1 class="page_error page_margin_top_section">Error 404</h1>

<div class="row-fluid page_margin_top clearfix"> 		

<p style="font-size: 16px;padding:0; color:#666;">Looks like the page you are looking for does not exists.<br>

If you came here from a bookmark, please remember to update your bookmark</p>

 </div>

 <div class="home_pages"><a href="<?php echo $this->baseurl; ?>/index.php" title="<?php echo JText::_('JERROR_LAYOUT_GO_TO_THE_HOME_PAGE'); ?>" class="btn btn-primary page_margin_top  fleft"><i class="fa fa-angle-left"></i>Go Back To Homepage</a></div>

</div>





</body>

</html>

