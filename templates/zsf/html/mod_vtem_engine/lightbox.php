<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
$navigation = $params->get('slideshow-nav', 'dots');
$navAlign = $params->get('slideshow-nav-align', 'left');
$progressbar = $params->get('slideshow-progressbar', 0);
$slideshoweffect = $params->get('slideshow-effect', 'random');
?>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery.colorbox.js';?>"></script>
<script type="text/javascript">
var vtemengine = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('#vtem-<?php echo $module_id;?>-lightbox > dt > a.vtem-lightbox').colorbox({
		inline: true,
		rel: 'vtem-lightbox',
		retinaImage: true, 
		width:  '<?php echo $width;?>',
		height: '<?php echo $height == 'auto' ? '50%' : $height;?>',
		maxWidth: '95%',
		maxHeight: '95%',
		slideshow: <?php echo $auto_play;?>,
		slideshowSpeed: <?php echo $autoplay_delay;?>,
		slideshowStart: '<?php echo JText::_('MOD_VTEM_ENGINE_LINGTBOX_START');?>',
		slideshowStop: '<?php echo JText::_('MOD_VTEM_ENGINE_LINGTBOX_STOP');?>',
		current: '<?php echo JText::_('MOD_VTEM_ENGINE_LINGTBOX_CURRENT');?>'
	}); 
});
</script>

<div id="vtemlightbox<?php echo $module_id;?>" class="vtem_lightbox_<?php echo $navigation;?> vtem_lightbox clearfix lightbox<?php echo $params->get('moduleclass_sfx');?>">
        <dl id="vtem-<?php echo $module_id;?>-lightbox" class="lightbox-data">
        	<?php if($beforetext != '') echo '<div class="vtem-before-text col-md-3 col-sm-4 col-xs-6 block ">'.$beforetext.'</div>';?>
            <?php
			if($content_source == "images"){
				foreach($images as $key => $img):
				      $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
					  $vtlinks = explode(";",$params->get('urls'));
					  $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
						  echo '<dt class="col-md-4 col-sm-4 col-xs-6 block"><a class="vtem-lightbox image" href="#vtem-lightbox'.$key.'"><img class="vt_lightbox_thumb" src="'.JURI::root().$imagePath.$img.'" alt="VTEM Lightbox" /><span class="fa fa-search-plus">&nbsp;</span></a></dt>
						   <dd style="display:none"><div id="vtem-lightbox'.$key.'">';
									  if($linkedimage == 1){
									   echo '<a href="'.trim($vtlink).'" target="'.$linktarget.'"><img class="vt_lightbox_main" src="'.JURI::root().$imagePath.$img.'" alt="VTEM Lightbox" /></a>';
									   }else{
									   echo '<img class="vt_lightbox_main" src="'.$imagePath.$img.'" alt="VTEM Lightbox" />';
									   }
									   if($showcaption == 1){
										   echo '<div class="label_text">
												 <h4 class="vtem_skitter_title">'.trim($vttitle).'</h4>
												 <div>'.trim($vtcontent).'</div>
											 </div>';
									   }
							  echo '</div>
						  </dd>';
				endforeach;
		}else{
                for($i=0; $i<count($list); $i++){
                    if($list[$i]->introtext != NULL){
                        echo '<dt><a class="vtem-lightbox" href="#vtem-lightbox'.$i.'">'.$list[$i]->title.'</a></dt><dd style="display:none"><div id="vtem-lightbox'.$i.'">'.$list[$i]->introtext.'</div></dd>';
                    }
                }
		}
                ?>
                
         <?php if($aftertext != '') echo '<div class="vtem-after-text col-md-3 col-sm-4 col-xs-6 block">'.$aftertext.'</div>';?>       
                
        </dl>
</div>
