<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
$scrollerAnimation= $params->get('scroller-animation', 'slide');
$scrollerNav = $params->get('scroller-nav', 1);
$scrollerDirection = $params->get('scroller-direction', 'horizontal');
$scrollerMaxitem = $params->get('scroller-maxitem', 1);
?>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery.oscroller.js';?>"></script>
<script type="text/javascript">
var vtemengine = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('#vtemscroller<?php echo $module_id;?>').oScroller({
		width: '<?php echo $width;?>',
		height: '<?php echo $height;?>', 
		effect: '<?php echo $scrollerAnimation;?>', //slide, fade
		autoplay: <?php echo $auto_play;?>,
		interval: <?php echo $autoplay_delay;?>,
		hoverpause: <?php echo $pauseonhover;?>,
		pager: <?php echo $scrollerNav;?>,
		nextPrev: <?php echo $prevnext;?>,
	}); 
});
</script>
<?php if($beforetext != '') echo '<div class="vtem-before-text">'.$beforetext.'</div>';?>
<div id="vtemscroller<?php echo $module_id;?>" class="vtem_scroller_<?php echo $scrollerDirection;?> vtem_scroller clearfix scroller<?php echo $params->get('moduleclass_sfx');?>">
<div class="bg_slides top"></div>
    <ul id="vtem-<?php echo $module_id;?>-scroller" class="slides">
<?php		$counter=0;
			if($content_source == "images"){
				foreach($images as $key => $img):
				      $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
					  $vtlinks = explode(";",$params->get('urls'));
					  $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
					  $rowcount =((int)$key % (int)$scrollerMaxitem)+1;
						  if($rowcount == 1) echo '<li id="vtem'.$key.'" class="'.$scrollerDirection.' clearfix">';
						  		echo '<div class="'.($scrollerDirection == 'horizontal' ? 'col-md-'.round(12/$scrollerMaxitem) : 'col-md-12').'">';
						          if($linkedimage == 1){
						           echo '<a href="'.trim($vtlink).'" target="'.$linktarget.'"><img class="vt_skitter_thumb" src="'.JURI::root().$imagePath.$img.'" alt="VTEM skitter" /></a>';
								   }else{
								   echo '<img class="vt_skitter_thumb" src="'.$imagePath.$img.'" alt="VTEM skitter" />';
								   }
								   if($showcaption == 1){
								       echo '<div class="label_text">
											 <h4 class="vtem_skitter_title">'.trim($vttitle).'</h4>
											 <div>'.trim($vtcontent).'</div>
										 </div>';
								   }
								 echo '</div>';
						  if( $rowcount == $scrollerMaxitem || $counter == count($images)) echo '</li>';
				endforeach;
		}else{
                for($i=0; $i<count($list); $i++){
					$rowcount =((int)$i % (int)$scrollerMaxitem)+1;
                    if($list[$i]->introtext != NULL){
                        if($rowcount == 1) echo '<li id="vtem'.$i.'" class="'.$scrollerDirection.' clearfix"><div class="row">';
						echo '<div class="'.($scrollerDirection == 'horizontal' ? 'col-md-'.round(12/$scrollerMaxitem) : 'col-md-12').' block">'.$list[$i]->introtext.'</div>';
						$counter++;
						if( $rowcount == $scrollerMaxitem || $counter == count($list)) echo '</div></li>';
                    }
                }
		}
?>
    </ul>
<div class="bg_slides bottom"></div>    
</div>

<?php if($aftertext != '') echo '<div class="vtem-after-text">'.$aftertext.'</div>';?>