<?php
/**
* @Copyright Copyright (C) 2010 VTEM . All rights reserved.
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @link     	http://www.vtem.net
**/
$title_type = $params->get('mod_title_type');
?>
<script type="text/javascript" src="<?php echo JURI::root(true).'/modules/'.$module->module.'/styles/jquery.otab.js';?>"></script>
<script type="text/javascript">
var vtemtabs = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('#vtem<?php echo $module_id;?>-tabs').oTab({ 
		width:  '<?php echo $width;?>',
		height: '<?php echo $height;?>',
		effect: '<?php echo $transition_type;?>', 
		nextPrev: <?php echo $prevnext;?>,
		hoverpause: <?php echo $pauseonhover;?>,
		tabEvent: '<?php echo $mouseEvent;?>',
		autoplay: <?php echo $auto_play;?>,
		duration: <?php echo $autoplay_delay;?>, 
		speed: 500,
		tabActiveClass: 'active-tab' 
	}); 
});
</script>
<?php if($beforetext != '') echo '<div class="vtem-before-text">'.$beforetext.'</div>';?>
<div id="vtem<?php echo $module_id;?>-tabs" class="vtem-tabs-wrapper vtem-slides-wrapper clearfix vtemtabs-<?php echo $modstyle;?> module<?php echo $moduleclass_sfx;?>">
        <?php if($tabs_position == 'top' && $content_source != "images") echo $tabs_title;?>
        <?php if($tabs_position == 'top' && $content_source == "images"){
				$html  = "<div class='clearfix vtemtabs-nav vtemtabs-nav-$tabs_position'>";
				$html .= "<ul class='vtemtabsnav oTabs nav nav-tabs'>";
				foreach($images as $key => $img):
					$class = '';
						if(!$key) $class= 'first';
						if($key == count($images) - 1) $class= 'last';
						if($title_type == 'custom'){
							$titles = explode(";",$params->get('mod_title_custom'));
							$title = (isset($titles[$key])) ? $titles[$key] : '';
						}else $title = 'Tab'.($key+1);
						$html .= "<li class='$class'><a href='#'><span>$title</span><strong>&nbsp;</strong></a></li>\n";

				endforeach;
				$html .= "</ul>\n";
				$html .= "</div>";
				echo $html;
		}?>
        <div class="vtem-tabs oPanels clearfix tabs-<?php echo $tabs_position;?> tabs-nextprev<?php echo $prevnext;?>">
            <?php
			if($content_source == "images"){
				foreach($images as $key => $img):
				      $vttitles = explode(";",$params->get('imagetitle'));
					  $vttitle = (isset($vttitles[$key])) ? $vttitles[$key] : '';
					  $vtcontents = explode(";",$params->get('imagecontent'));
					  $vtcontent = (isset($vtcontents[$key])) ? $vtcontents[$key] : '';
					  $vtlinks = explode(";",$params->get('urls'));
					  $vtlink = (isset($vtlinks[$key])) ? $vtlinks[$key] : '';
						  echo "<div class='vtemtabs-panel'><div class='vtemtabs-item clearfix'>\n";
						          if($linkedimage == 1){
						           echo '<a href="'.trim($vtlink).'" target="'.$linktarget.'"><img class="vt_skitter_thumb" src="'.JURI::root().$imagePath.$img.'" alt="VTEM skitter" /></a>';
								   }else{
								   echo '<img class="vt_skitter_thumb" src="'.$imagePath.$img.'" alt="VTEM skitter" />';
								   }
								   if($showcaption == 1){
								       echo '<div class="label_text">
											 <h4 class="vtem_skitter_title">'.trim($vttitle).'</h4>
											 <div>'.trim($vtcontent).'</div>
										 </div>';
								   }
						  echo "</div></div>\n";
				endforeach;
		}else{
                for($i=0; $i<count($list); $i++){
                    if($list[$i]->introtext != NULL){
                        echo "<div class='vtemtabs-panel'><div class='vtemtabs-item clearfix'>\n";
                            echo $list[$i]->introtext;?>
                        <?php echo "</div></div>\n";
                    }
                }
		}
                ?>
        </div>
        <?php if($tabs_position == 'bottom' && $content_source != "images") echo $tabs_title;?>
        <?php if($tabs_position == 'bottom' && $content_source == "images"){
				$html  = "<div class='clearfix vtemtabs-nav vtemtabs-nav-$tabs_position'>";
				$html .= "<ul class='vtemtabsnav oTabs nav nav-tabs'>";
				foreach($images as $key => $img):
					$class = '';
						if(!$key) $class= 'first';
						if($key == count($images) - 1) $class= 'last';
						if($title_type == 'custom'){
							$titles = explode(";",$params->get('mod_title_custom'));
							$title = (isset($titles[$key])) ? $titles[$key] : '';
						}else $title = 'Tab'.($key+1);
						$html .= "<li class='$class'><a href='#'><span>$title</span><strong>&nbsp;</strong></a></li>\n";

				endforeach;
				$html .= "</ul>\n";
				$html .= "</div>";
				echo $html;
		}?>
</div>
<?php if($aftertext != '') echo '<div class="vtem-after-text">'.$aftertext.'</div>';?>