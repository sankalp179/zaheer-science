<!DOCTYPE html>
<?php
/* Design by VTEM http://www.vtem.net  All Rights Reserved */
defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')){define('DS',DIRECTORY_SEPARATOR);}
$vtOutout = '';
$vtOutout .= '<html xml:lang="'.$this->language.'" lang="'.$this->language.'">';
include_once(dirname(__FILE__).DS.'vtemtools'.DS.'default.php');
include_once(dirname(__FILE__).DS.'vtemtools'.DS.'css_var.php');
$vtOutout .= '<body id="vtem" class="'.$pageoption.' opt-'.$pageview.' menuid'.$pageID.' template-'.$TemplateStyle.' logo-'.$logo.' '.css_browser_selector().'">';
$vtOutout .= 	'<div id="vt_wrapper" class="vt_wrapper vt_container">';
                    ///// VTEMShowModule('Position Name', 'Position style', 'Custom Class')
$vtOutout .=        VTEMShowModule('drawer', 'vt_xhtml', 'container').
                    VTEMShowModule('header', 'vt_xhtml', 'container').
                    VTEMShowModule('top', 'vt_xhtml', 'container').
                    VTEMShowModule('showcase', 'vt_xhtml', 'container');
$vtOutout .= 	'<div class="container"><div class="vt_wrapper_box_main">';					
$vtOutout .=        '<main class="vt_main clearfix">'.$component.'</main>';
$vtOutout .=        VTEMShowModule('feature', 'vt_xhtml', 'vt_container').
                    VTEMShowModule('bottom', 'vt_xhtml', 'vt_container');
$vtOutout .=	'</div></div>';	
	
$vtOutout .= 	'<div class="container">';					
$vtOutout .=        VTEMShowModule('footer', 'vt_xhtml', 'vt_container');				
$vtOutout .=        '<footer class="vt_wrapper_copyright vt_section">'.
            			'<div class="vt_container clearfix">'.
                			'<div class="col-md-12">'.($totop ? '<a id="gotop" href="#vtem" title="Back to Top"><span><i class="fa fa-angle-up">&nbsp;</i></span></a>' : '').$vtcopyright.
                 			'</div>'.
             			'</div>'.
        			'</footer>';
$vtOutout .=	'</div></div>';	   
include_once(dirname(__FILE__).DS.'vtemtools'.DS.'debug.php');
$vtOutout .='</body>';
$vtOutout .='</html>';
echo $vtOutout;
?>