<?php
defined('_JEXEC') or die('Restricted access');
///  Renderer modules  //////////////////////////////////////////////////////////////////////////////////////////
function VTEMShowModule($name, $style, $customClass = '', $compos = -1){
	$modmodule = $modstep = $showlogo = $showmenu = $vtExtPosition = $vtDesktop = $vtTablet = $vtPhone = $vtCustomClass = '';
	jimport( 'joomla.application.module.helper' );
	$customParams = JFactory::getApplication()->getTemplate(true)->params;
	$doc = JFactory::getDocument();
	$app = JFactory::getApplication();
	
	//// MENU /////////////////////////////////////////////////
	$menucontrol = $customParams->get("menucontrol", 1);
	$menu_name = $customParams->get("menutype", "mainmenu");
	$startLevel = $customParams->get("startLevel", 0);
	$endLevel = $customParams->get("endLevel", 10);
	$renderer	= $doc->loadRenderer('module');
	$module = JModuleHelper::getModule('mod_menu', '$menu_name');
	$attribs['style'] = 'none';
	$module->params	= "menutype=$menu_name\nshowAllChildren=1\nstartLevel=$startLevel\nendLevel=$endLevel\nclass_sfx=vtem_nav mega-menu\ntag_id=vtemdefaultmenu";
	if($menucontrol == 1)
		$showmenu = '<div id="vt_main_menu" class="vt_menufix clearfix">'.$renderer->render($module, $attribs).'</div>';
	
		
	//// LOGO /////////////////////////////////////////////////
	if($customParams->get('logo') == 2)
		$showlogo .= '<a id="vt_logo" href="'.JURI::root().'"><h1>'.$customParams->get('logotext').'</h1><span>'.$customParams->get('slogan').'</span></a>';
	elseif($customParams->get('logo') == 1)
		$showlogo .= '<a id="vt_logo" href="'.JURI::root().'"><img src="'.$customParams->get('logoimage').'" alt="Logo" /></a>';
	else
		$showlogo .= '<a id="vt_logo" href="'.JURI::root().'"><img src="'.JURI::root().'templates/'.$app->getTemplate().'/css/styles/vt_logo_'.$customParams->get('TemplateStyle').'.png" alt="Logo" /></a>';
	
	
	//// MODULES /////////////////////////////////////////////////
	$logopos = $customParams->get('logopos', 'header1');
	$menupos = $customParams->get('menupos', 'header2');
	$posValue = explode("|",$customParams->get($name, '1|'.$name+'1:12/0/0/0/'));
	$posCount = $posValue[0];
	$posName = (isset($posValue[1]) ? $posValue[1] : $name+'1:12/0/0/0/');
	$modulecount = 0;
	for ($i = 0; $i < $posCount; $i++) {
		$vtCountPositions = explode(",", $posName);
		$vtCountPosition = (isset($vtCountPositions[$i])) ? $vtCountPositions[$i] : '';
		if($vtCountPosition != ''){
			$vtPositionName = explode(":", $vtCountPosition);
			$vtPositionNameValue = $vtPositionName[0];
			if(isset($vtPositionName[1]) != ''){
				$vtExtPosition = explode("/", $vtPositionName[1]);
				if (isset($vtExtPosition[0])) $vtPositionNameGrid = $vtExtPosition[0];
				if (isset($vtExtPosition[1])) $vtDesktop = $vtExtPosition[1];
				if (isset($vtExtPosition[2])) $vtTablet = $vtExtPosition[2];
				if (isset($vtExtPosition[3])) $vtPhone = $vtExtPosition[3];
				if (isset($vtExtPosition[4])) $vtCustomClass = $vtExtPosition[4];
			}
		}
		if (count(JModuleHelper::getModules($vtPositionNameValue)) || $logopos == ($vtPositionNameValue) || $menupos == ($vtPositionNameValue) || ($i == $compos && $compos >= 0)) :
			$modmodule .='<div class="col-md-'.$vtPositionNameGrid.' '.($vtDesktop ? 'hidden-desktop hidden-md hidden-lg' : '').' '.($vtTablet ? 'hidden-tablet hidden-sm' : '').' '.($vtPhone ? 'hidden-phone hidden-xs' : '').' '.$vtCustomClass.' '.($i > 0 ? 'separator_'.$name : '').' '.(($i == $compos && $compos >= 0) ? 'vt_component' : 'vt_block').'">';
			$modmodule .=($logopos == $vtPositionNameValue ? $showlogo : '').($menupos == $vtPositionNameValue ? $showmenu : '');
			$modmodule .=(($i == $compos && $compos >= 0) ? '<jdoc:include type="message" /><jdoc:include type="component" />' : '');
				if (count(JModuleHelper::getModules($vtPositionNameValue)))
					$modmodule .='<jdoc:include type="modules" name="'.$vtPositionNameValue.'" style="'.$style.'" />';
			$modmodule .='</div>';
			$modulecount = $modulecount + 1;
		endif;
	}
	if($modulecount > 0)
		$modmodule = '<section class="vt_wrapper_'.$name.' vt_section"><div class="'.$customClass.' vt_group"><div id="vt_'.$name.'" class="vt_'.$name.' row-fluid clearfix">'
						.$modmodule.
					 '</div></div></section>';
	
return $modmodule;
}
///  Cookies  //////////////////////////////////////////////////////////////////////////////////////////
$cookie_prefix = $this->template;
$cookie_time = time()+30000000;
$vtem_temp = array('TemplateStyle','Layout');
foreach ($vtem_temp as $tprop) {
    $vtem_session = JFactory::getSession();
	
	if (isset($_REQUEST[$tprop])) {
	    $$tprop = JRequest::getString($tprop, null, 'get');
    	$vtem_session->set($cookie_prefix.$tprop, $$tprop);
    	setcookie ($cookie_prefix. $tprop, $$tprop, $cookie_time, '/', false);   
    	global $$tprop; 
    }
}
$pageview = JRequest::getVar('view', '');
$pageoption = JRequest::getVar('option', '');
$pageID = JRequest::getVar('Itemid', '');
$template_baseurl = $this->baseurl.'/templates/'.$this->template;
$Default_TemplateStyle 	= $this->params->get("TemplateStyle", "style1");
$Default_Layout	= $this->params->get("layout", "lbr");
$copyright = $this->params->get("copyright", 1);
$SystemMessages = $this->params->get("SystemMessages", 1);
$IE6Warning = $this->params->get("IE6Warning", 1);
$GoogleAnalytics = $this->params->get("GoogleAnalytics", 0);
$gacode = $this->params->get("gacode", 'UA-17014902-1');
$logo = $this->params->get("logo", 0);
$menuTrigger = $this->params->get("menuTrigger", "hover");
$menuEffect = $this->params->get("menuEffect", "fade");
$menuStick = $this->params->get("menuStick", 1);
//FEATURES
$fontfamily = $this->params->get("fontfamily", 'Arial, Helvetica, sans-serif');
$fontsize = $this->params->get("fontsize", '12px');
$totop = $this->params->get("totop", 1);
$responsive = $this->params->get("responsive", 1);
$jquery = $this->params->get("jquery", 0);
$webfont = $this->params->get("webfont", 0);
$googlefont = $this->params->get("googlefont", 'Thin');
$googlefontelements = $this->params->get("googlefontelements", 'h3');
$googlefont1 = explode(":", $googlefont);
$document	= JFactory::getDocument();
$jversion = new JVersion;
$document->addStyleSheet($template_baseurl.'/vtemtools/menus/css/styles.css');
$document->addStyleSheet($template_baseurl.'/css/font-awesome/css/font-awesome.min.css');
if ($jquery == 1) 
	$document->addScript($template_baseurl.'/vtemtools/widgets/jquery-1.7.2.min.js');
if ($jquery == 2){
	if (version_compare($jversion->getShortVersion(), '3.0.0', '<'))
		$document->addScript($template_baseurl.'/vtemtools/widgets/jquery-1.7.2.min.js');
}
$document->addScript($template_baseurl.'/css/bootstrap/js/bootstrap.min.js');
$document->addScript($template_baseurl.'/vtemtools/widgets/browser-detect.js');
require_once (dirname(__FILE__).DS.'switcher.php');
require_once (dirname(__FILE__).DS.'css_browser_selector.php');
?>